const Discord = require('discord.js');
const {
	discord_token
} = require('./config.json');

var cards = ["2a", "2b", "2c", "2d",
"3a", "3b", "3c", "3d",
"4a", "4b", "4c", "4d",
"5a", "5b", "5c", "5d",
"6a", "6b", "6c", "6d",
"7a", "7b", "7c", "7d",
"8a", "8b", "8c", "8d",
"9a", "9b", "9c", "9d",
"10a", "10b", "10c", "10d", "10e", "10f", "10g", "10h", "10i", "10j", "10k", "10l", "10m", "10n", "10o", "10p",
"11a", "11b", "11c", "11d"]

picked = []

player_cards = []
player_score = 0
joueur_joue = 1

dealer_cards = []
dealer_score = 0

player_money = 500
dealer_money = 500

player = "none"
channel = "none"

turn = 0
lost = 0
started = 0

function pick() {
	picked_card = cards[Math.floor((Math.random() * cards.length) + 0)]
	if (picked.indexOf(picked_card) != -1) {
		pick()
	} else {
		picked.push(picked_card)
		turning()
	}
}

function turning() {
	if (turn == 0) {
		player_cards.push(picked_card)
		card_value = translate()
		player_score += card_value
		card_type = genre()
		channel.send(player + " a pioché: " + card_value + card_type)
		channel.send(`Score actuel du joueur: ${player_score}\n`)
		if (player_score > 21) {
			channel.send("Le joueur a perdu à cause d'une main d'une valeur supérieure à 21 ! (" + player_score + ")")
			picked_card = dealer_cards[0]
			first_card_value = translate()
			first_card_type = genre()
			picked_card = dealer_cards[1]
			second_card_value = translate()
			second_card_type = genre()
			channel.send("Les deux premières cartes du dealer étaient: " + first_card_value + first_card_type + " Puis " + second_card_value + second_card_type)
			player_money -= 100
			dealer_money += 100
			lost = 1
			started = 0
		} else {
			if (picked.length <= 4) {turn += 1}
			blacking()
		}
	} else if (turn == 1) {
		dealer_cards.push(picked_card)
		card_value = translate()
		dealer_score += card_value
		card_type = genre()
		if (dealer_score > 21) {
			channel.send("Le dealer a perdu à cause d'une main d'une valeur supérieure à 21 ! (" + dealer_score + ")")
			player_money += 100
			dealer_money -= 100
			lost = 1
			started = 0
		} else {
			if (joueur_joue == 1) {turn -= 1}
			blacking()
		}
	}
}

function blacking() {
	if (picked.length == 4) {
		if (player_score == 21) {
			if (dealer_score != 21) {
				channel.send("Blackjack de la part du joueur grâce aux deux premières cartes !")
				picked_card = dealer_cards[0]
				first_card_value = translate()
				first_card_type = genre()
				picked_card = dealer_cards[1]
				second_card_value = translate()
				second_card_type = genre()
				channel.send("Les deux premières cartes du dealer étaient: " + first_card_value + first_card_type + " Puis " + second_card_value + second_card_type)
				started = 0
				lost = 1
				player_money += 100
				dealer_money -= 100
			} else {
				console.log("Blackjack du joueur et du dealer, match nul !\n\n\n")
				started = 0
			}
		} else {
			console.log("Pas de Blackjack pour le moment.\n")
		}
	} else if (lost == 0 && picked.length < 4) {
		pick()
	}
}

function translate() {
	if (picked_card.length <= 2) {
		return Math.round(picked_card.slice(0, 1))
	} else if (Math.round(picked_card.slice(0, 2)) == 10) {
		return 10
	} else {
		if (turn == 0) {
			if (player_score + 11 <= 21) {
				return 11
			} else {
				return 1
			}
		} else if (turn == 1) {
			if (dealer_score + 11 <= 21) {
				return 11
			} else {
				return 1
			}
		}
	}
}

function genre() {
	if (picked_card.slice(-1) == "a") {return " de Piques."}
	if (picked_card.slice(-1)== "b") {return " de Carreaux."}
	if (picked_card.slice(-1) == "c") {return " de Coeurs."}
	if (picked_card.slice(-1) == "d") {return " de Trèfles."}
	if (picked_card.slice(-1) == "e") {return " de Piques. (VALET)"}
	if (picked_card.slice(-1) == "f") {return " de Carreaux. (VALET)"}
	if (picked_card.slice(-1) == "g") {return " de Coeurs. (VALET)"}
	if (picked_card.slice(-1) == "h") {return " de Trèfles. (VALET)"}
	if (picked_card.slice(-1) == "i") {return " de Piques. (DAME)"}
	if (picked_card.slice(-1) == "j") {return " de Carreaux. (DAME)"}
	if (picked_card.slice(-1) == "k") {return " de Coeurs. (DAME)"}
	if (picked_card.slice(-1) == "l") {return " de Trèfles. (DAME)"}
	if (picked_card.slice(-1) == "m") {return " de Piques. (ROI)"}
	if (picked_card.slice(-1) == "n") {return " de Carreaux. (ROI)"}
	if (picked_card.slice(-1) == "o") {return " de Coeurs. (ROI)"}
	if (picked_card.slice(-1) == "p") {return " de Trèfles. (ROI)"}
}

function discord_login() {
	client = new Discord.Client()
	client.login(discord_token)
	.catch(console.error)
	client.once('ready', () => {
		discord_ready = "true"
		console.log("\nConnecté à Discord !\n")
	});
	client.on('error', error => {
		console.error('(Blackjack DB, Discord) The websocket connection encountered an error:', error);
	});
	client.on('message', msg => {
		if (started == 0) {
			if (msg.content === ']blackjack') {
				if (player_money <= 0 || dealer_money <= 0) {
					if (player_money <= 0) {
						msg.reply("vous avez épuisé tout votre argent ! Retentez votre chance une autre fois !")
						setTimeout(process.exit, 2500)
					}
					if (dealer_money <= 0) {
						msg.reply("vous avez épuisé l'argent du casino, félicitations ! Revenez bientôt !")
						setTimeout(process.exit, 2500)
					}
				} else {
					picked = []

					player_cards = []
					player_score = 0
					joueur_joue = 1

					dealer_cards = []
					dealer_score = 0

					turn = 0
					lost = 0
					started = 1
					player = msg.author
					channel = msg.channel
					channel.send("L'argent du casino: " + dealer_money)
					channel.send("L'argent du joueur: " + player_money)
					channel.send ("Le montant d'argent en jeu est 100")
					msg.reply("la partie de Blackjack a commencé !")
					pick()
					if (dealer_cards.length == 2) {channel.send("La deuxième carte du dealer est: " + card_value + card_type)} else {console.log("a")}
				}
			}
		} else {
			if (msg.author == player && msg.channel == channel) {
				if (msg.content === ']hit') {
					msg.reply("vous piochez une carte.")
					pick()
				}
				if (msg.content === ']stand') {
					joueur_joue = 0
					picked_card = dealer_cards[0]
					first_card_value = translate()
					first_card_type = genre()
					channel.send("La première carte du dealer était: " + first_card_value + first_card_type)
					while (dealer_score < 17) {
						turn = 1
						pick()
						first_card_value = translate()
						first_card_type = genre()
						channel.send("Le dealer a pioché: " + first_card_value + first_card_type)
					}
					if (dealer_score >= 17 && dealer_score <= 21) {
						if (dealer_score > player_score) {
							channel.send(`Le dealer gagne avec une main de ${dealer_score} contre ${player} qui a une main de ${player_score} !`)
							player_money -= 100
							dealer_money += 100
						} else if (dealer_score < player_score) {
							channel.send(`${player} gagne avec une main de ${player_score} contre le dealer qui a une main de ${dealer_score} !`)
							player_money += 100
							dealer_money -= 100
						} else {
							channel.send(`Egalité !`)
						}
						started = 0
					}
				}
				if (msg.content === ']surrender') {
					channel.send(player + " capitule, donnant à la banque 50 d'argent.")
					player_money -= 50
					dealer_money += 50
					started = 0
				}
			}
		}
	});
}

discord_login()
